package space.chamoy.fotai.api;

import android.os.AsyncTask;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import space.chamoy.fotai.Globals;

/**
 * Created by jesus on 21/02/16.
 */
public class ApiUsersNearbyConsumer extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... params) {

        try{
            URL url = new URL(params[0]);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.addRequestProperty("lat", params[1]);
            connection.addRequestProperty("long", params[2]);
            connection.addRequestProperty("radius", params[3]);

            connection.setRequestMethod("POST");
            connection.setRequestProperty("User-Agent", Globals.userAgent);
            connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            String urlParameters = "lat="+params[1]+"&long="+params[2]+"&radius="+params[3];
            // Send post request
            connection.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            int responseCode = connection.getResponseCode();

            InputStream input = connection.getInputStream();
            int c;
            StringBuffer buffer = new StringBuffer();
            while((c = input.read()) != -1){
                buffer.append((char)c);
            }
            return buffer.toString();
        }catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected void onCancelled(String s) {
        super.onCancelled(s);
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    public ApiUsersNearbyConsumer() {
        super();
    }
}
