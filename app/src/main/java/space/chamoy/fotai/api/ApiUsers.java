package space.chamoy.fotai.api;


import android.app.ProgressDialog;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import space.chamoy.fotai.app.AppController;
import space.chamoy.fotai.structures.FoodTruck;
import space.chamoy.fotai.structures.User;

public class ApiUsers{

    public static User getVolleyUser(int id){
        // Tag used to cancel the request
        String tag_json_obj = "json_obj_req";

        String url = ApiGlobals.mainUrl + "/users/get/" + Integer.toString(id);

        final User user = new User();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            user.setId(response.getInt("id"));
                            user.setFirstname(response.getString("firstname"));
                            user.setLastname(response.getString("lastname"));
                            user.setLat(response.getDouble("lat"));
                            user.setLng(response.getDouble("long"));
                            user.setToken(response.getString("token"));
                            user.setEmail(response.getString("email"));

                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               // TODO error
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
        return user;
    }

    public static User getUser(int id){
        ApiUsersConsumer consumer = new ApiUsersConsumer();
        try {
            String s = consumer.execute(ApiGlobals.mainUrl + "/users/get/" + Integer.toString(id)).get();
            JSONObject object = new JSONObject(s);
            User user = new User();
            user.setId(object.getInt("id"));
            user.setFirstname(object.getString("firstname"));
            user.setLastname(object.getString("lastname"));
            user.setLat(object.getDouble("lat"));
            user.setLng(object.getDouble("long"));
            user.setToken(object.getString("token"));
            user.setEmail(object.getString("email"));
            return user;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<User> getNearestUsers(LatLng latLng){
        ApiUsersNearbyConsumer consumer = new ApiUsersNearbyConsumer();

        ArrayList<User> users = new ArrayList<>();
        try {
            String s = consumer.execute(ApiGlobals.mainUrl + "/users/nearby", Double.toString(latLng.latitude), Double.toString(latLng.longitude), Float.toString(5.0f)).get();
            JSONObject response = new JSONObject(s);
            JSONArray trucks = response.getJSONArray("trucks");
            for (int i = 0; i < trucks.length(); i++) {
                JSONObject truck = trucks.getJSONObject(i);
                User ft = new User();
                ft.setLat(truck.getDouble("lat"));
                ft.setLng(truck.getDouble("long"));
                users.add(ft);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return users;
    }
}
