package space.chamoy.fotai.api;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import space.chamoy.fotai.structures.FoodTruck;

/**
 * Created by jesus on 20/02/16.
 */
public class ApiFoodTruck {
    public static ArrayList<FoodTruck> getFoodTrucksNerby(double lat, double lng, float radius){
        ApiFoodTruckConsumer consumer = new ApiFoodTruckConsumer();
        ArrayList<FoodTruck> foodTrucks = new ArrayList<>();
        try {
            String s = consumer.execute(ApiGlobals.mainUrl + "/foodtrucks/nearby", Double.toString(lat), Double.toString(lng), Float.toString(radius)).get();
            JSONObject response = new JSONObject(s);
            JSONArray trucks = response.getJSONArray("trucks");
            for (int i = 0; i < trucks.length(); i++) {
                JSONObject truck = trucks.getJSONObject(i);
                FoodTruck ft = new FoodTruck();
                ft.setLat(truck.getDouble("lat"));
                ft.setLng(truck.getDouble("long"));
                ft.setNombre(truck.getString("name"));
                foodTrucks.add(ft);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return foodTrucks;
    }
}
