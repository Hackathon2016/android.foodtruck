package space.chamoy.fotai.api;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by jesus on 20/02/16.
 */
public class ApiUsersConsumer extends AsyncTask<String, Void, String> {
    public ApiUsersConsumer() {
        super();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled(String s) {
        super.onCancelled(s);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    @Override
    protected String doInBackground(String... params) {
            try{
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();

                InputStream input = connection.getInputStream();
                int c;
                StringBuffer buffer = new StringBuffer();
                while((c = input.read()) != -1){
                    buffer.append((char)c);
                }
                return buffer.toString();
            }catch(Exception e){
                e.printStackTrace();
            }
        return null;
    }


}
