package space.chamoy.fotai.ai;

import java.util.Random;

public class NeuralNetwork {
    private int patternCount = 10;
    private int inputNodes = 7;
    private int hiddenNodes = 8;
    private int outputNodes = 4;
    private double LearningRate = 0.3;
    private double Momentum = 0.9;
    private double initialWeightMax = 0.5;
    private double success = 0.0004;

    private int input[][] = {
        { 1, 1, 1, 1, 1, 1, 0 },  // 0
        { 0, 1, 1, 0, 0, 0, 0 },  // 1
        { 1, 1, 0, 1, 1, 0, 1 },  // 2
        { 1, 1, 1, 1, 0, 0, 1 },  // 3
        { 0, 1, 1, 0, 0, 1, 1 },  // 4
        { 1, 0, 1, 1, 0, 1, 1 },  // 5
        { 0, 0, 1, 1, 1, 1, 1 },  // 6
        { 1, 1, 1, 0, 0, 0, 0 },  // 7
        { 1, 1, 1, 1, 1, 1, 1 },  // 8
        { 1, 1, 1, 0, 0, 1, 1 }   // 9
    };

    private int target[][] = {
            { 0, 0, 0, 0 },
            { 0, 0, 0, 1 },
            { 0, 0, 1, 0 },
            { 0, 0, 1, 1 },
            { 0, 1, 0, 0 },
            { 0, 1, 0, 1 },
            { 0, 1, 1, 0 },
            { 0, 1, 1, 1 },
            { 1, 0, 0, 0 },
            { 1, 0, 0, 1 }
    };


    int i, j, p, q, r;
    int reportEvery1000;
    int randomizedIndex[] = new int[patternCount];
    double rando;
    double error;
    double accum;


    double hidden[] = new double[hiddenNodes];
    double output[] = new double[outputNodes];
    double hiddenWeights[][] = new double[inputNodes+1][hiddenNodes];
    double outputWeights[][] = new double[hiddenNodes+1][outputNodes];
    double hiddenDelta[] = new double[hiddenNodes];
    double outputDelta[] = new double[outputNodes];
    double changeHiddenWeights[][] = new double[inputNodes+1][hiddenNodes];
    double changeOutputWeights[][] = new double[hiddenNodes+1][outputNodes];

    Random random = new Random();

    public NeuralNetwork(){
        reportEvery1000 = 1;
        for( p = 0 ; p < patternCount ; p++ ) {
            randomizedIndex[p] = p ;
        }
    }

    public void trainNetwork() {
        for (i = 0; i < hiddenNodes; i++) {
            for (j = 0; j <= inputNodes; j++) {
                changeHiddenWeights[j][i] = 0.0;

                rando = random.nextInt(100) * 1.0 / 100;
                hiddenWeights[j][i] = 2.0 * (rando - 0.5) * initialWeightMax;
            }
        }

        for (i = 0; i < outputNodes; i++) {
            for (j = 0; j <= hiddenNodes; j++) {
                changeOutputWeights[j][i] = 0.0;
                rando = random.nextInt(100) * 1.0 / 100;
                outputWeights[j][i] = 2.0 * (rando - 0.5) * initialWeightMax;
            }
        }

        for (long trainingCycle = 1; trainingCycle < 2147483647; trainingCycle++) {


            for (p = 0; p < patternCount; p++) {
                q = random.nextInt(patternCount);
                r = randomizedIndex[p];
                randomizedIndex[p] = randomizedIndex[q];
                randomizedIndex[q] = r;
            }
            error = 0.0;

            for (q = 0; q < patternCount; q++) {
                p = randomizedIndex[q];


                for (i = 0; i < hiddenNodes; i++) {
                    accum = hiddenWeights[inputNodes][i];
                    for (j = 0; j < inputNodes; j++) {
                        accum += input[p][j] * hiddenWeights[j][i];
                    }
                    hidden[i] = 1.0 / (1.0 + Math.exp(-accum));
                }


                for (i = 0; i < outputNodes; i++) {
                    accum = outputWeights[hiddenNodes][i];
                    for (j = 0; j < hiddenNodes; j++) {
                        accum += hidden[j] * outputWeights[j][i];
                    }
                    output[i] = 1.0 / (1.0 + Math.exp(-accum));
                    outputDelta[i] = (target[p][i] - output[i]) * output[i] * (1.0 - output[i]);
                    error += 0.5 * (target[p][i] - output[i]) * (target[p][i] - output[i]);
                }


                for (i = 0; i < hiddenNodes; i++) {
                    accum = 0.0;
                    for (j = 0; j < outputNodes; j++) {
                        accum += outputWeights[i][j] * outputDelta[j];
                    }
                    hiddenDelta[i] = accum * hidden[i] * (1.0 - hidden[i]);
                }


                for (i = 0; i < hiddenNodes; i++) {
                    changeHiddenWeights[inputNodes][i] = LearningRate * hiddenDelta[i] + Momentum * changeHiddenWeights[inputNodes][i];
                    hiddenWeights[inputNodes][i] += changeHiddenWeights[inputNodes][i];
                    for (j = 0; j < inputNodes; j++) {
                        changeHiddenWeights[j][i] = LearningRate * input[p][j] * hiddenDelta[i] + Momentum * changeHiddenWeights[j][i];
                        hiddenWeights[j][i] += changeHiddenWeights[j][i];
                    }
                }


                for (i = 0; i < outputNodes; i++) {
                    changeOutputWeights[hiddenNodes][i] = LearningRate * outputDelta[i] + Momentum * changeOutputWeights[hiddenNodes][i];
                    outputWeights[hiddenNodes][i] += changeOutputWeights[hiddenNodes][i];
                    for (j = 0; j < hiddenNodes; j++) {
                        changeOutputWeights[j][i] = LearningRate * hidden[j] * outputDelta[i] + Momentum * changeOutputWeights[j][i];
                        outputWeights[j][i] += changeOutputWeights[j][i];
                    }
                }
            }
            if (error < success) break;
        } // training cycle
    }

    private double[] solve(double []input){
        output = new double[this.outputNodes];
        for (i = 0; i < hiddenNodes; i++) {
            accum = hiddenWeights[inputNodes][i];
            for (j = 0; j < inputNodes; j++) {
                accum += input[j] * hiddenWeights[j][i];
            }
            hidden[i] = 1.0 / (1.0 + Math.exp(-accum));
        }

        for (i = 0; i < outputNodes; i++) {
            accum = outputWeights[hiddenNodes][i];
            for (j = 0; j < hiddenNodes; j++) {
                accum += hidden[j] * outputWeights[j][i];
            }
            output[i] = 1.0 / (1.0 + Math.exp(-accum));
        }
        return output;
    }
}
