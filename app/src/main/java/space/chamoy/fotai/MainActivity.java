package space.chamoy.fotai;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import space.chamoy.fotai.api.ApiFoodTruck;
import space.chamoy.fotai.api.ApiGlobals;
import space.chamoy.fotai.api.ApiUsers;
import space.chamoy.fotai.structures.FoodTruck;
import space.chamoy.fotai.structures.User;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, LocationListener {
    private GoogleMap mMap;
    private LatLng currentLatLng;
    private LocationManager locationManager;
    private String provider;
    private boolean ready;
    private SharedPreferences sharedPreferences;
    private Location lastKnownLocation = null;
    private float detectionRadius;
    private ArrayList<FoodTruck> foodtrucks;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lastKnownLocation = getLastKnownLocation();
        sharedPreferences = getSharedPreferences(Globals.sharedPreferences, MODE_PRIVATE);
        detectionRadius = sharedPreferences.getFloat("radius", 5.0f);
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        provider = locationManager.getBestProvider(new Criteria(), false);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        new Runnable() {
            @Override
            public void run() {
                Random random = new Random();
                final User user = ApiUsers.getUser(random.nextInt(98) + 1);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((TextView) findViewById(R.id.menu_email)).setText(user.getEmail());
                        ((TextView) findViewById(R.id.menu_name)).setText(user.getFirstname() + " " + user.getLastname());
                    }
                });
                setCurrentPin(new LatLng(user.getLat(), user.getLng()));
                setFoodTrucksNerby();
            }
        }.run();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        ready = true;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onLocationChanged(Location location) {
//        lastKnownLocation = location;
//        if(ready){
//            double lat = location.getLatitude();
//            double lng = location.getLongitude();
//
//            final LatLng latLng = new LatLng(lat, lng);
//
//            new Runnable() {
//                @Override
//                public void run() {
//                    setCurrentPin(latLng);
//                    setFoodTrucksNerby();
//                }
//            }.run();
//        }
    }

    private void setCurrentPin(LatLng latLng){
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(latLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
    }

    private void setFoodTrucksNerby(){
        LatLng latLng = new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
        setCurrentPin(latLng);
        float radius = sharedPreferences.getFloat("radius", 5.0f);
        foodtrucks = ApiFoodTruck.getFoodTrucksNerby(latLng.latitude, latLng.longitude, radius);
        for(FoodTruck foodTruck : foodtrucks){
            LatLng latLng1 = new LatLng(foodTruck.getLat(), foodTruck.getLng());
            mMap.addMarker(new MarkerOptions().position(
                    new LatLng(foodTruck.getLat(), foodTruck.getLng())
            ).title(foodTruck.getNombre())
            .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin)));
        }
    }

    private Location getLastKnownLocation() {
        locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        Location l = null;
        for (String provider : providers) {

            try {
                l = locationManager.getLastKnownLocation(provider);
            } catch(SecurityException e){
                e.printStackTrace();
            }

            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }
}
